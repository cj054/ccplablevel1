//Implement and execute a program to interchange the largest and smallest element in a given array
#include<stdio.h>
int input()
{
	int n;
	printf("Enter the number of elements-");
	scanf("%d",&n);
	return n;
}
void input_array(int n,int a[n])
{
	printf("Enter the elements-");
	for(int i=0;i<n;i++)
	scanf("%d",&a[i]);
}
int find_small(int n,int a[n])
{
	int small=a[0],minpos=0;
	for(int i=1;i<n;i++)
	{
		if(a[i]<small)
		minpos=i;
	}
	return minpos;
}
int find_large(int n,int a[n])
{
	int large=a[0],maxpos=0;
	for(int i=1;i<n;i++)
	{
		if(a[i]>large)
		maxpos=i;
	}
	return maxpos;
}
void swap(int maxpos,int minpos,int a[])
{
	int temp;
	temp=a[minpos];
	a[minpos]=a[maxpos];
	a[maxpos]=temp;
}
void output(int n,int a[n])
{
	printf("The new array=\n");
	for(int i=0;i<n;i++)
	printf("%d\t",a[i]);
}
int main()
{
	int n,small,large;
	n=input();
	int a[n];
	input_array(n,a);
	small=find_small(n,a);
	large=find_large(n,a);
	swap(large,small,a);
	output(n,a);
	return 0;
}	
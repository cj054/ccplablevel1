#include<stdio.h>
#include<math.h>
struct roots
{
    float real;
    float complex;
}; 
typedef struct roots root;
void input(int *a,int *b,int *c)
{
    printf("Enter the value of a,b,c-\n");
    scanf("%d %d %d",a,b,c);
}
void find_roots(int a,int b,int c,root *r)
{
	float d=(b*b)-(4*a*c);
	if(d>0)
	{
		r->real=(-b)/(2*a);
		r->complex=sqrt(d);
	}
	else if(d==0)
	{
		r->real=(-b)/(2*a);
	}
	else
	{
	    r->complex=sqrt(-d);
		r->real=(-b)/(2*a);
	}
}
void output(int a,int b,int c,root r)
{
    float d=(b*b)-(4*a*c);
    if(d>0)
    printf("Roots are real\nRoots=%f+%f and %f-%f",r.real,r.complex,r.real,r.complex);
    else if(d==0)
    printf("Roots are real and equal\nRoot=%f",r.real);
    else
    printf("Roots are imaginary\nRoots=%f+%fi and %f-%fi",r.real,r.complex,r.real,r.complex);
}
int main()
{
	int a,b,c;
	root r;
	input(&a,&b,&c);
	find_roots(a,b,c,&r);
	output(a,b,c,r);
	return 0;
}
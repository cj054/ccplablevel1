/*Demonstratehow to read data from the keyboard, write it to a file called INPUT,
again read the same data from the INPUT file, and display it on the screen/console.*/
#include<stdio.h>
void output_error()
{
    printf("File doesn't exist\n");
}
void output_success()
{
    printf("Successful\n");
}
void input_content(char *ch,FILE *fp)
{
    printf("Enter the contents of the file-\n");
    while((*ch=getchar())!='\n')
    {
        fputc(*ch,fp);
    }
}
void output_content(char ch,FILE *fp)
{
    printf("\nContents of file is :\n");
    while((ch=fgetc(fp))!=EOF)
    {
        printf("%c",ch); 
    }
}
int main()
{
    FILE *fp;   
    char ch;
    fp=fopen("INPUT.txt","w");
    if(fp==NULL)
    {
        output_error();
    }
    else
    {
        output_success();
    }
    input_content(&ch,fp);
    output_success();
    fclose(fp);
    fp=fopen("INPUT.txt","r");
    if(fp==NULL)
    {
        output_error();
        return 0;
    }
    output_content(ch,fp); 
    fclose(fp);
    return 0;
}
//A program to calculate the average of n elements using arrays
#include<stdio.h>
int input()
{
	int n;
	printf("Enter the number of array elements-");
	scanf("%d",&n);
	return n;
}
void input_array(int n,int a[n])
{
	int i;
	printf("Enter the elements-");
	for(i=0;i<n;i++)
	scanf("%d",&a[i]);
}
int find_sum(int n,int a[n])
{
	int sum=0;
	for(int i=0;i<n;i++)
	sum=sum+a[i];
	return sum;
}
float find_avg(int sum,int n)
{
	float avg=sum/n;
	return avg;
}
int output(float avg,int n,int a[n])
{
	printf("Average of the elements-\n");
    for(int i=0;i<n;i++)
    {
        printf("%d ",a[i]);
    }
    printf("=%f",avg);
	return 0;
}	
int main()
{
	int n,sum;
	float avg;
	n=input();
	int a[n];
	input_array(n,a);
	sum=find_sum(n,a);
	avg=find_avg(sum,n);
	output(avg,n,a);
	return 0;
}	
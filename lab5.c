//Develop a C program to determine the month given input in the range 1-12 using switch case statement 
#include<stdio.h>
int input()
{
	int m;
	printf("Enter the month in numbers-");
	scanf("%d",&m);
	return m;
}
char* compute(int m)
{
	switch(m)
	{
		case 1:
		{
			return "January";
		}
		case 2:
		{
			return "February";
		}
		case 3:
		{
			return "March";
		}
		case 4:
		{
			return "April";
		}
		case 5:
		{
			return "May";
		}
		case 6:
		{
			return "June";
		}
		case 7:
		{
			return "July";
		}
		case 8:
		{
			return "August";
		}
		case 9:
		{
			return "September";
		}
		case 10:
		{
			return "October";
		}
		case 11:
		{
			return "November";
		}
		case 12:
		{
			return "December";
		}
		default:
		{
			return "Wrong_Input";
		}
		
	}	
}
void output(char* month)
{
    printf("The month=%s",month);
}
int main()
{
	int m;
	char* month;
	m=input();
	month=compute(m);
	output(month);
	return 0;
}	
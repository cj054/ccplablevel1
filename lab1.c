#include<stdio.h>
int input_principal()
{
    int p;
    printf("Enter the principal amount-");
    scanf("%d",&p);
    return p;
}
float input_time()
{
    float t;
    printf("Enter the time period-");
    scanf("%f",&t);
    return t;
}
float input_rate()
{
    float r;
    printf("Enter the rate of interest-");
    scanf("%f",&r);
    return r;
}
float find_si(int p,float t,float r)
{
    float si=(p*t*r)/100;
    return si;
}
int output(float si)
{
    printf("The Simple Interest=%f",si);
    return 0;
}
int main()
{
    int p;
    float r,t,si;
    p=input_principal();
    t=input_time();
    r=input_rate();
    si=find_si(p,t,r);
    output(si);
    return 0;
}